ruby-rubyvis (0.6.1+dfsg1-3) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove unnecessary 'Testsuite: autopkgtest' header.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-rspec.
  * Bump debhelper from old 12 to 13.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 07:23:10 +0530

ruby-rubyvis (0.6.1+dfsg1-2) unstable; urgency=medium

  * Use RSpec 3 (Closes: #795049)
  * Fix double space in debian/control

 -- Balint Reczey <balint@balintreczey.hu>  Mon, 17 Aug 2015 23:09:57 +0200

ruby-rubyvis (0.6.1+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #786911)

 -- Balint Reczey <balint@balintreczey.hu>  Fri, 22 May 2015 18:08:07 +0200
